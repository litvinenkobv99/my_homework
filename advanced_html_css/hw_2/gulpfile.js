const gulp = require("gulp");
const sass = require("gulp-sass")(require("sass"));
const clean = require("gulp-clean");
const purgecss = require("gulp-purgecss");
const concat = require("gulp-concat");
const cleanCSS = require("gulp-clean-css");
const minifyjs = require("gulp-js-minify");
const rename = require("gulp-rename");
const browserSync = require("browser-sync").create();
const tinypng = require("gulp-tinypng");
const autoprefixer = require("gulp-autoprefixer");

gulp.task("tinypng", () => {
  return gulp
    .src("./src/images/**")
    .pipe(tinypng("Tf3xFMPYlz0r2SsdsXwQtghJcLspQYym"))
    .pipe(gulp.dest("dist/images"));
});
gulp.task("buildStyles", function () {
  return gulp
    .src("./src/scss/style.scss")
    .pipe(sass().on("error", sass.logError))
    .pipe(gulp.dest("./src/css"));
});
gulp.task("autoprefixer", function () {
  return gulp
    .src("src/css/style.css")
    .pipe(
      autoprefixer({
        overrideBrowserList: ["last 2 versions"],
        cascade: false,
      })
    )
    .pipe(gulp.dest("./src/css"));
});
gulp.task("clean", function () {
  return gulp.src("./dist/*", { read: false }).pipe(clean());
});
gulp.task("purgecss", () => {
  return gulp
    .src("src/**/*.css")
    .pipe(
      purgecss({
        content: ["*.html"],
      })
    )
    .pipe(gulp.dest("src"));
});
gulp.task("concatCSS", function () {
  return gulp
    .src(["./src/css/**/*.css"])
    .pipe(concat("style.css"))
    .pipe(gulp.dest("./src/css/"));
});
gulp.task("minify-css", () => {
  return gulp
    .src("./src/css/**/*.css")
    .pipe(cleanCSS({ compatibility: "ie8" }))
    .pipe(rename("style.min.css"))
    .pipe(gulp.dest("./dist/css"));
});
gulp.task("concatJS", function () {
  return gulp
    .src(["./src/js/index.js"])
    .pipe(concat("main.js"))
    .pipe(gulp.dest("./src/js/concat/"));
});
gulp.task("minify-js", function () {
  return gulp
    .src("./src/js/index.js")
    .pipe(minifyjs())
    .pipe(rename("scripts.min.js"))
    .pipe(gulp.dest("./dist/js"));
});

gulp.task(
  "build",
  gulp.series([
    "clean",
    "buildStyles",
    "autoprefixer",
    "purgecss",
    "concatCSS",
    "minify-css",
    "concatJS",
    "minify-js",
    "tinypng",
  ])
);
gulp.task("dev", () => {
  browserSync.init({
    server: "./",
  });
  gulp.watch(
    "./src/scss/**/**.scss",
    gulp.series(
      "buildStyles",
      "autoprefixer",
      "purgecss",
      "concatCSS",
      "minify-css"
    )
  );
  gulp.watch("./src/js/index.js", gulp.series("concatJS", "minify-js"));
  gulp.watch("./index.html").on("change", browserSync.reload);
  gulp.watch("./dist/css/style.min.css").on("change", browserSync.reload);
  gulp.watch("./dist/js/scripts.min.js").on("change", browserSync.reload);
});

const books = [
  {
    author: "Скотт Бэккер",
    name: "Тьма, что приходит прежде",
    price: 70,
  },
  {
    author: "Скотт Бэккер",
    name: "Воин-пророк",
  },
  {
    name: "Тысячекратная мысль",
    price: 70,
  },
  {
    author: "Скотт Бэккер",
    name: "Нечестивый Консульт",
    price: 70,
  },
  {
    author: "Дарья Донцова",
    name: "Детектив на диете",
    price: 40,
  },
  {
    author: "Дарья Донцова",
    name: "Дед Снегур и Морозочка",
  },
];
const root = document.querySelector("#root");
const list = document.createElement("ul");

root.append(list);

function inputList() {
  books.forEach((item) => {
    try {
      if (!item.name) {
        throw new SyntaxError("Данные неполны: нет имени");
      }
      if (!item.author) {
        throw new SyntaxError("Данные неполны: нет автора");
      }
      if (!item.price) {
        throw new SyntaxError("Данные неполны: нет цены");
      }
      let itemList = document.createElement("li");
      itemList.innerText = item.author + " " + item.name + " " + item.price;
      list.append(itemList);
    } catch (e) {
      console.log(e);
    }
  });
}

inputList();

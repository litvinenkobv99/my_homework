import Card from "./Сard.js";
import getUsers from "./getUsers.js";
import getPosts from "./getPosts.js";

function displayPosts() {
  getUsers().then((users) => {
    getPosts().then((posts) => {
      users.forEach((user) => {
        posts.forEach((post) => {
          if (user.id === post.userId) {
            let card = new Card(
              user.name,
              user.email,
              post.title,
              post.body,
              post.id
            );
            card.render();
          }
        });
      });
    });
  });
}

displayPosts();

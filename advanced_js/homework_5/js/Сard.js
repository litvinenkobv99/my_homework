export default class Card {
  constructor(name, email, title, text, id) {
    this.name = name;
    this.email = email;
    this.title = title;
    this.text = text;
    this.id = id;
  }
  createElement(elemType, classNames, text) {
    const element = document.createElement(elemType);
    if (text) element.textContent = text;
    element.classList.add(...classNames);
    return element;
  }
  renderHeader(container) {
    const header = this.createElement("div", ["card-header"]);
    header.insertAdjacentHTML(
      "afterbegin",
      `<a href=# class= 'card-name'> ${this.name} </a>
        <div class="btn-del"> <svg fill="#000000" xmlns="http://www.w3.org/2000/svg" 
        viewBox="0 0 24 24" width="24px" height="24px">
        <path d="M 10.806641 2 C 10.289641 2 9.7956875 2.2043125 9.4296875 2.5703125 
        L 9 3 L 4 3 A 1.0001 1.0001 0 1 0 4 5 L 20 5 A 1.0001 1.0001 0 1 0 20 3 L 15 3 
        L 14.570312 2.5703125 C 14.205312 2.2043125 13.710359 2 13.193359 2 L 10.806641 
        2 z M 4.3652344 7 L 5.8925781 20.263672 C 6.0245781 21.253672 6.877 22 7.875 22 L
         16.123047 22 C 17.121047 22 17.974422 21.254859 18.107422 20.255859 L 19.634766 
         7 L 4.3652344 7 z"></path></svg></div>  
        <div class="card-email"> ${this.email} </div>`
    );
    container.append(header);
    header.addEventListener("click", (e) => {
      const deleteTarget = e.target.closest(".btn-del");
      if (deleteTarget) {
        const toDelUser = confirm("Delete post?");
        if (toDelUser) {
          this.card.remove();
          fetch(`https://ajax.test-danit.com/api/json/posts/${this.id}`, {
            method: "DELETE",
          });
        }
      }
    });
  }
  renderText(container) {
    const text = this.createElement("div", ["card-body"]);
    text.insertAdjacentHTML(
      "afterbegin",
      `<div class="card-title"> ${this.title} </div>  <div class="card-text"> ${this.text} </div>`
    );
    container.append(text);
  }
  render() {
    this.card = this.createElement("div", ["card-container"]);
    document.querySelector(".wrapper").append(this.card);
    this.renderHeader(this.card);
    this.renderText(this.card);
    return this.card;
  }
}

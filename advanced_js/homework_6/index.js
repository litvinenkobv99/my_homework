const btn = document.querySelector(".btn");

btn.addEventListener("click", async (e) => {
  const response = await fetch("https://api.ipify.org/?format=json");
  const result = await response.json();
  const response_1 = await fetch(`http://ip-api.com/json/${result.ip}`);
  const result_2 = await response_1.json();
  addressOutput(result_2);
});

function addressOutput(place) {
  const div = document.createElement("div");
  div.style = `position: absolute;
                left:45%;
                top: 55%;`;
  document.body.append(div);
  div.insertAdjacentHTML(
    "beforeend",
    `
  <div>Country: ${place.country}</div>
  <div>City: ${place.city}</div>
  <div>Country code: ${place.countryCode}</div>
  <div>Region: ${place.region}</div>
  <div>Region name: ${place.regionName}</div>
  <div>Timezone: ${place.timezone}</div>
  `
  );
}

const formPass = document.querySelector(".password-form");
const btn = document.querySelector(".btn");
const enterPass = document.querySelector("#enter-password");
const confirmPass = document.querySelector("#confirm-password");
const error = document.createElement("span");
const fa_eye_slash = document.querySelectorAll(".fa-eye-slash");

error.innerHTML = "Нужно ввести одинаковые значения";
error.style.color = "red";

formPass.addEventListener("click", (e) => {
  if (e.target.classList.contains("icon-password")) {
    showPassword();
  }
});

btn.addEventListener("click", (e) => {
  e.preventDefault();
  checkValue();
});

function showPassword() {
  fa_eye_slash.forEach((elem) => elem.classList.toggle("hidden"));
  if (
    enterPass.getAttribute("type") == "password" &&
    confirmPass.getAttribute("type") == "password"
  ) {
    enterPass.setAttribute("type", "text");
    confirmPass.setAttribute("type", "text");
  } else {
    enterPass.setAttribute("type", "password");
    confirmPass.setAttribute("type", "password");
  }
}

function checkValue() {
  if (
    enterPass.value === confirmPass.value &&
    enterPass.value !== "" &&
    confirmPass.value !== ""
  ) {
    error.remove();
    alert("You are welcome");
  } else {
    btn.before(error);
  }
}

const btn = document.querySelectorAll(".btn");

document.addEventListener("keydown", (e) => {
  btn.forEach((elem) => {
    // let result =
    //   e.code == elem.dataset.letter
    //     ? elem.classList.add("color")
    //     : elem.classList.remove("color");
    // можно было сделать через тернарный оператор,
    // но это лишняя переменная которая, к тому же, нигде не используется
    if (e.code == elem.dataset.letter) {
      elem.classList.add("color");
    } else {
      elem.classList.remove("color");
    }
  });
});

const imgs = document.querySelectorAll(".image-to-show");
const btn = document.querySelector(".btn");
const resumeShow = document.querySelector(".resume-show");
let arr = Array.from(imgs);
let timer = setInterval(showImg, 3000, arr);

function showImg(arr) {
  for (const e of arr) {
    if (!e.classList.contains("hide-img")) {
      e.classList.add("hide-img");
      // if (arr.indexOf(e) !== arr.length - 1) {
      //   e.nextElementSibling.classList.remove("hide-img");
      //   break;
      // } else {
      //   arr[0].classList.remove("hide-img");
      // }
    }
  }
}

btn.addEventListener("click", (e) => {
  clearTimeout(timer);
});
resumeShow.addEventListener("click", (e) => {
  timer = setInterval(showImg, 3000, arr);
});

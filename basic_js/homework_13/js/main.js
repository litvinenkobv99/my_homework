const btnChangeTheme = document.querySelector(".change-theme");
const body = document.querySelector("body");
const popularPosts = document.querySelector(".popular_posts");
const headerBackgroundLeftButton = document.querySelector(
  ".header_background_left_button"
);
const headerBackgroundRightButton = document.querySelector(
  ".header_background_right_button"
);

btnChangeTheme.addEventListener("click", (e) => {
  if (
    localStorage.getItem("color_theme") === null ||
    localStorage.getItem("color_theme") === "white"
  ) {
    localStorage.setItem("color_theme", "black");
    toggleClass();
  } else {
    if (localStorage.getItem("color_theme") === "black") {
      localStorage.setItem("color_theme", "white");
      toggleClass();
    }
  }
});

window.addEventListener("load", (e) => {
  if (localStorage.getItem("color_theme") === "black") {
    toggleClass();
  }
});

function toggleClass() {
  body.classList.toggle("black_theme");
  popularPosts.classList.toggle("black_theme");
  headerBackgroundLeftButton.classList.toggle("black_theme");
  headerBackgroundRightButton.classList.toggle("black_theme");
}

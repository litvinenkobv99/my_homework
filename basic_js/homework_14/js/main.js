$(document).ready(function () {
  $(window).scroll(function () {
    if (scrollY > $(window).innerHeight()) {
      $(".to-top").addClass("to-top-show");
    } else {
      $(".to-top").removeClass("to-top-show");
    }
  });
  $(".to-top").click(function () {
    $("body, html").animate({ scrollTop: 0 }, 1000, "swing");
  });
  $(document).on("click", ".slide", function () {
    $(".popular_clients").toggle("slow");
  });
});

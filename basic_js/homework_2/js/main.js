let number = null;
do {
  number = +prompt("Please enter the number: ");
} while (!Number.isInteger(number));

for (let i = 0; i < number; i++) {
  if (number < 5) {
    console.log("Sorry, no numbers");
    break;
  } else {
    if (i % 5 == 0) {
      console.log(i);
    }
  }
}

let m = null;
let n = null;

do {
  m = +prompt("Please enter the number m: ");
  n = +prompt("Please enter the number n: ");
} while (!Number.isInteger(m) || !Number.isInteger(n));

label1: for (m; m <= n; m++) {
  for (let j = 2; j < m; j++) {
    if (m % j == 0) continue label1;
  }
  console.log(m);
}

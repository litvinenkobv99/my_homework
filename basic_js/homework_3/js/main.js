let number1;
let number2;
let symbol;
let answer;

do {
  number1 = +prompt("Please enter the number 1: ");
  number2 = +prompt("Please enter the number 2: ");
  symbol = prompt("enter symbol");
} while (
  !Number.isInteger(+number1) ||
  !Number.isInteger(+number2) ||
  number1 == "" ||
  number2 == ""
);

function operation(number1, number2, symbol) {
  switch (symbol) {
    case "+":
      return number1 + number2;
    case "-":
      return number1 - number2;
    case "*":
      return number1 * number2;
    case "/":
      return number1 / number2;
    default:
      console.log("Введен не верный символ");
  }
}

answer = operation(number1, number2, symbol);

if (answer === undefined) {
  console.log("Операция не выполнена!");
} else {
  console.log(`Ответ : ${answer}`);
}

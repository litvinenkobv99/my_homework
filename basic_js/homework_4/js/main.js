function createNewUser() {
  const newUser = {
    firstName: prompt("Enter your name"),
    lastName: prompt("Enter your last name"),
    getLogin: () =>
      (newUser.firstName.substring(0, 1) + newUser.lastName).toLowerCase(),
  };
  return newUser;
}

const user = createNewUser();
const userLogin = user.getLogin();
console.log(user);
console.log(userLogin);

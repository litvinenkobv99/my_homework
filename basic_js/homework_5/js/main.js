function createNewUser() {
  const newUser = {
    firstName: prompt("Enter your name"),
    lastName: prompt("Enter your last name"),
    birthday: new Date(prompt("Enter your date of birth(year, month, date)")),
    getLogin: function () {
      return (this.firstName.substring(0, 1) + this.lastName).toLowerCase();
    },
    getAge: function () {
      return new Date().getFullYear() - this.birthday.getFullYear();
    },
    getPassword: function () {
      return (
        this.firstName.substring(0, 1).toUpperCase() +
        this.lastName.toLowerCase() +
        this.birthday.getFullYear()
      );
    },
  };
  return newUser;
}

const user = createNewUser();
const userLogin = user.getLogin();
const userAge = user.getAge();
const userPassword = user.getPassword();

console.log(user);
console.log(userLogin);
console.log(userAge);
console.log(userPassword);

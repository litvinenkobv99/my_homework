const array = ["hello", "world", 23, "23", null];

function filterBy(array, dataType) {
  let result;
  if (dataType === "null") {
    result = array.filter((e) => e !== null);
    return result;
  } else {
    result = array.filter((e) => typeof e !== dataType);
    return result;
  }
}

console.log(filterBy(array, "null"));

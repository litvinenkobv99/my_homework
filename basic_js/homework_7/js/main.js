const array = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
const list = document.body;

function outputArrayElements(arr, list) {
  list = document.createElement("ul");
  document.body.prepend(list);
  let newArray = arr.map(function (item, index, array) {
    let li = document.createElement("li");
    li.innerText = item;
    list.append(li);
    return li;
  });
}

outputArrayElements(array, list);

const body = document.querySelector("body");
const price = document.createElement("div");
const inputPrice = document.createElement("input");

price.innerHTML = "Price: ";

let span;
let btn;
let error;

body.prepend(price);
price.append(inputPrice);

inputPrice.addEventListener("focus", (e) => {
  inputPrice.style.cssText = `
  outline: none;
  border: 1px solid green;
  border-radius: 2px;`;
});

inputPrice.addEventListener("blur", (e) => {
  inputPrice.style.cssText = `
  border: 1px solid black;
  border-radius: 2px;`;
  if (inputPrice.value <= 0 || isNaN(inputPrice.value)) {
    inputPrice.style.backgroundColor = "red";
    if (error === undefined) {
      error = document.createElement("span");
      error.innerHTML = "Please enter correct price";
      body.append(error);
    } else {
      body.append(error);
    }
  } else {
    if (inputPrice.value > 0) {
      if (error !== undefined) {
        error.remove();
      }
      inputPrice.style.backgroundColor = "green";
      createElem(inputPrice.value);
    }
  }
});

function createElem(val) {
  span = document.createElement("span");
  btn = document.createElement("button");

  btn.style.cssText = `border-radius: 50%;
    border:1px solid black;
    cursor: pointer;`;

  btn.innerHTML = `X`;
  span.innerHTML = `  Текущая цена: ${val}  `;

  span.append(btn);
  body.prepend(span);
  deleteElem();
}

function deleteElem() {
  btn.addEventListener("click", (e) => {
    e.target.closest("span").remove();
  });
}

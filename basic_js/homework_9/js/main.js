const tabsContentItems = document.querySelectorAll(".tabs-content-item");
const tabs = document.querySelector(".tabs");
let currentTab = document.querySelector(".active");
let currentItem = document.querySelector(".active-item");

tabs.addEventListener("click", (e) => {
  currentTab.classList.remove("active");
  e.target.classList.add("active");
  currentTab = e.target;

  for (const iterator of tabsContentItems) {
    if (e.target.dataset.name === iterator.dataset.name) {
      currentItem.classList.remove("active-item");
      iterator.classList.add("active-item");
      currentItem = iterator;
    }
  }
});

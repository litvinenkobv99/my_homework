const arrModalWindow = [
  {
    id: 0,
    title: "Do you want to delete this file?",
    text: "Once you delete this file, it won't possible to undo this action. Are you sure you want to delete it?",
    closeButton: true,
    actions: (
      <>
        <button className="modal__button-ok">OK</button>
        <button className="modal__button-cancel">Cancel</button>
      </>
    ),
  },
  {
    id: 1,
    title: "Lorem ipsum dolor sit amet.",
    text: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illo ex perspiciatis maiores dolores et cum magni ipsum reprehenderit nam quis.",
    closeButton: true,
    actions: (
      <>
        <button className="modal__button-ok">Continue</button>
        <button className="modal__button-cancel">Back</button>
      </>
    ),
  },
];
export default arrModalWindow;

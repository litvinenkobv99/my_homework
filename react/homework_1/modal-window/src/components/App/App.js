import React from "react";
import Button from "../Button/Button";
import Modal from "../Modal/Modal";
import "../../nullstyles.css";
import arrModalWindow from "../../arrModalWindow.js";

class App extends React.Component {
  constructor() {
    super();
    this.state = {
      isOpen: false,
      arrModalWindow: {},
    };
  }

  openModal = (id) => {
    this.setState({
      isOpen: true,
      arrModalWindow: {
        ...arrModalWindow[id],
      },
    });
  };
  closeModal = () => {
    this.setState({
      isOpen: false,
    });
  };
  render() {
    const { id, title, text, closeButton, actions } = this.state.arrModalWindow;
    return (
      <>
        <div className="button">
          <Button
            text="Open first modal"
            style={{ backgroundColor: "purple" }}
            openModal={this.openModal}
            dataModalId={0}
          />
          <Button
            text="Open second modal"
            style={{ backgroundColor: "blue" }}
            openModal={this.openModal}
            dataModalId={1}
          />
        </div>
        {this.state.isOpen && (
          <Modal
            id={id}
            title={title}
            text={text}
            closeButton={closeButton}
            actions={actions}
            closeModal={this.closeModal}
          />
        )}
      </>
    );
  }
}

export default App;

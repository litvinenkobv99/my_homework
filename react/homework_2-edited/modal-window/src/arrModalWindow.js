const arrModalWindow = [
  {
    id: 0,
    title: "Додати товар до кошику?",
    text: "",
    closeButton: true,
    // actions: (
    //   <>
    //     <button className="modal__button-ok">OK</button>
    //     <button className="modal__button-cancel">Cancel</button>
    //   </>
    // ),
  },
  {
    id: 1,
    title: "Lorem ipsum dolor sit amet.",
    text: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illo ex perspiciatis maiores dolores et cum magni ipsum reprehenderit nam quis.",
    closeButton: true,
    actions: (
      <>
        <button className="modal__button-ok">Continue</button>
        <button className="modal__button-cancel">Back</button>
      </>
    ),
  },
];
export default arrModalWindow;

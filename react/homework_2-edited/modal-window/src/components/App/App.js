import React from "react";
import Button from "../Button/Button";
import Modal from "../Modal/Modal";
import "../../nullstyles.css";
import arrModalWindow from "../../arrModalWindow.js";
import Header from "../Header/Header";
import ProductList from "../ProductList/ProductList";

class App extends React.Component {
  constructor() {
    super();
    this.state = {
      isOpen: false,
      arrModalWindow: {},
      arrProducts: [],
      arrFavoriteProducts: [],
      arrProductsToBuy: [],
    };
  }
  componentDidMount = () => {
    fetch("/arrProducts.JSON")
      .then((response) => response.json())
      .then((result) => {
        this.setState({
          arrProducts: result,
        });
      });
    if (localStorage.getItem("arrFavoriteProducts") == null) {
      localStorage.setItem(
        "arrFavoriteProducts",
        JSON.stringify(this.state.arrFavoriteProducts)
      );
    } else {
      this.setState({
        arrFavoriteProducts: JSON.parse(
          localStorage.getItem("arrFavoriteProducts")
        ),
      });
    }

    if (localStorage.getItem("arrProductsToBuy") == null) {
      localStorage.setItem(
        "arrProductsToBuy",
        JSON.stringify(this.state.arrProductsToBuy)
      );
    } else {
      this.setState({
        arrProductsToBuy: JSON.parse(localStorage.getItem("arrProductsToBuy")),
      });
    }
  };
  openModal = (id, product) => {
    this.setState({
      isOpen: true,
      arrModalWindow: {
        ...arrModalWindow[id],
      },
      buyProduct: product,
    });
  };
  closeModal = () => {
    this.setState({
      isOpen: false,
    });
  };
  addProductsToBuy = () => {
    !this.state.arrProductsToBuy.includes(this.state.buyProduct) &&
      this.setState({
        arrProductsToBuy: [
          ...this.state.arrProductsToBuy,
          this.state.buyProduct,
        ],
      });

    this.closeModal();
  };
  addProductsToFavorites = (productToFavorites) => {
    if (!this.state.arrFavoriteProducts.includes(productToFavorites)) {
      this.setState({
        arrFavoriteProducts: [
          ...this.state.arrFavoriteProducts,
          productToFavorites,
        ],
      });
      console.log(1);
    } else {
      this.setState({
        arrFavoriteProducts: this.state.arrFavoriteProducts.filter(
          (product) => {
            if (JSON.stringify(product) == JSON.stringify(productToFavorites)) {
              return false;
            } else {
              return true;
            }
          }
        ),
      });
    }
  };
  componentDidUpdate = () => {
    localStorage.arrFavoriteProducts = JSON.stringify(
      this.state.arrFavoriteProducts
    );
    localStorage.arrProductsToBuy = JSON.stringify(this.state.arrProductsToBuy);
  };
  render() {
    const { id, title, text, closeButton, actions } = this.state.arrModalWindow;
    return (
      <>
        <Header
          arrFavoriteProducts={this.state.arrFavoriteProducts}
          arrProductsToBuy={this.state.arrProductsToBuy}
        />
        <ProductList
          arrProducts={this.state.arrProducts}
          openModal={this.openModal}
          addProductsToFavorites={this.addProductsToFavorites}
        />

        {this.state.isOpen && (
          <Modal
            id={id}
            title={title}
            text={text}
            closeButton={closeButton}
            actions={actions}
            closeModal={this.closeModal}
            addProductsToBuy={this.addProductsToBuy}
          />
        )}
      </>
    );
  }
}

export default App;

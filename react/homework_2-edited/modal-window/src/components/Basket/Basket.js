import { Component } from "react";
import "./Basket.scss";
import PropTypes from "prop-types";

class Basket extends Component {
  constructor() {
    super();
  }
  render() {
    return (
      <a href="#" className="header__basket">
        <img src="./img/shopping-cart.png" alt="" />
        {this.props.arrProductsToBuy.length}
      </a>
    );
  }
}

Basket.propTypes = {
  arrProductsToBuy: PropTypes.array,
};

export default Basket;

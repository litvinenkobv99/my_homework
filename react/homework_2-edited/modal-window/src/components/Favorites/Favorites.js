import { Component } from "react";
import "./Favorites.scss";
import PropTypes from "prop-types";

class Favorites extends Component {
  constructor() {
    super();
    this.state = {
      onClick: false,
    };
  }
  componentDidMount = () => {
    JSON.parse(localStorage.getItem("arrFavoriteProducts")).forEach(
      (element) => {
        if (
          JSON.stringify(element) ==
          JSON.stringify(this.props.productToFavorites)
        ) {
          this.setState({
            onClick: !this.state.onClick,
          });
        }
      }
    );
  };
  render() {
    const { addProductsToFavorites, productToFavorites } = this.props;
    return (
      <a
        onClick={() => {
          addProductsToFavorites(productToFavorites);
          this.setState({
            onClick: !this.state.onClick,
          });
        }}
        className="header__favorites"
      >
        {this.state.onClick ? (
          <img src="./img/favorite-check.png" />
        ) : (
          <img src="./img/favorite.png" alt="" />
        )}
      </a>
    );
  }
}

Favorites.propTypes = {
  addProductsToFavorites: PropTypes.func,
  product: PropTypes.object,
  productToFavorites: PropTypes.object,
};

export default Favorites;

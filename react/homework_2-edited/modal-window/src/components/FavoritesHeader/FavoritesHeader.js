import { Component } from "react";
import "./FavoritesHeader.scss";
import PropTypes from "prop-types";

class FavoritesHeader extends Component {
  constructor() {
    super();
  }
  render() {
    return (
      <a className="header__favorites" href="#">
        <img src="./img/favorite.png" alt="" />
        {this.props.arrFavoriteProducts.length}
      </a>
    );
  }
}

FavoritesHeader.propTypes = {
  arrFavoriteProducts: PropTypes.array,
};

export default FavoritesHeader;

import { Component } from "react";
import "./Header.scss";
import FavoritesHeader from "../FavoritesHeader/FavoritesHeader";
import Basket from "../Basket/Basket";
import PropTypes from "prop-types";

class Header extends Component {
  constructor() {
    super();
  }
  render() {
    return (
      <>
        <header className="header">
          <div className="header__logo">
            <img src="./img/logo.svg" alt="" />
          </div>
          <div>
            <Basket arrProductsToBuy={this.props.arrProductsToBuy} />
            <FavoritesHeader
              arrFavoriteProducts={this.props.arrFavoriteProducts}
            />
          </div>
        </header>
      </>
    );
  }
}

Header.propTypes = {
  arrFavoriteProducts: PropTypes.array,
  arrProductsToBuy: PropTypes.array,
};

export default Header;

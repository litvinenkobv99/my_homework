import { Component } from "react";
import "./ProductCard.scss";
import Button from "../Button/Button";
import Favorites from "../Favorites/Favorites";
import PropTypes from "prop-types";

class ProductCard extends Component {
  constructor() {
    super();
  }
  render() {
    const { name, price, url, vendorCode, color, handleClick } =
      this.props.product;
    return (
      <li className="products__product-item">
        <img className="products__product-item-img" src={url}></img>
        <div className="products__product-item-name"> {name}</div>
        <div className="products__product-item-price"> {price}₴</div>
        <div className="products__product-item-color">Колір: {color}</div>
        <div>
          <Favorites
            product={this.props.product}
            addProductsToFavorites={this.props.addProductsToFavorites}
            productToFavorites={this.props.product}
          />
          <Button
            text="Add to Card"
            style={{ backgroundColor: "purple" }}
            openModal={this.props.openModal}
            dataModalId={0}
            product={this.props.product}
          />
        </div>
      </li>
    );
  }
}

ProductCard.propTypes = {
  addProductsToFavorites: PropTypes.func,
  openModal: PropTypes.func,
  product: PropTypes.object,
};

export default ProductCard;

import { Component } from "react";
import "./ProductList.scss";
import ProductCard from "../ProductCard/ProductCard";
import PropTypes from "prop-types";

class ProductList extends Component {
  constructor() {
    super();
  }
  render() {
    const { arrProducts, openModal, addProductsToFavorites } = this.props;
    return (
      <ul
        className="products"
        style={{ maxWidth: "1200px", margin: "0 auto " }}
      >
        {arrProducts.map((product) => {
          return (
            <ProductCard
              key={product.vendorCode}
              product={product}
              openModal={openModal}
              addProductsToFavorites={addProductsToFavorites}
            />
          );
        })}
      </ul>
    );
  }
}

ProductList.propTypes = {
  arrProducts: PropTypes.array,
  openModal: PropTypes.func,
  addProductsToFavorites: PropTypes.func,
};

export default ProductList;

import React from "react";
import Button from "../Button/Button";
import Modal from "../Modal/Modal";
import ProductList from "../ProductList/ProductList";
import "../../nullstyles.css";
import arrModalWindow from "../../arrModalWindow.js";
import Header from "../Header/Header";

class App extends React.Component {
  constructor() {
    super();
    this.state = {
      isOpen: false,
      arrModalWindow: {},
      productsToBuy: [],
      buyProduct: {},
      countBuyProducts: 0,
    };
  }
  componentDidMount = () => {
    if (localStorage.getItem("productsToBuy")) {
      this.setState({
        productsToBuy: JSON.parse(localStorage.getItem("productsToBuy")),
      });
    }
  };
  openModal = (id, product) => {
    this.setState({
      isOpen: true,
      arrModalWindow: {
        ...arrModalWindow[id],
      },
      // ...productsToBuy,
      buyProduct: product,
    });
  };
  addProductsToBuy = () => {
    !this.state.productsToBuy.includes(this.state.buyProduct) &&
      this.setState({
        productsToBuy: [...this.state.productsToBuy, this.state.buyProduct],
      });
    console.log(this.state.productsToBuy);
    if (localStorage.getItem("productsToBuy") === null) {
      localStorage.setItem(
        "productsToBuy",
        JSON.stringify(this.state.productsToBuy)
      );
    } else {
      localStorage["productsToBuy"] = JSON.stringify(this.state.productsToBuy);
    }
    this.closeModal();
  };
  closeModal = () => {
    this.setState({
      isOpen: false,
    });
  };
  render() {
    const { id, title, text, closeButton, actions } = this.state.arrModalWindow;
    return (
      <>
        <Header countBuyProducts={this.state.countBuyProducts} />
        {this.state.isOpen && (
          <Modal
            id={id}
            title={title}
            text={text}
            closeButton={closeButton}
            actions={actions}
            closeModal={this.closeModal}
            addProductsToBuy={this.addProductsToBuy}
          />
        )}
        <ProductList openModal={this.openModal} />
      </>
    );
  }
}

export default App;

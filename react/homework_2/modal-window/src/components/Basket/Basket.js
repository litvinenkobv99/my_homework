import { Component } from "react";
import "./Basket.scss";
import PropTypes from "prop-types";

class Basket extends Component {
  constructor() {
    super();
    this.state = {
      productsToBuy: [],
    };
  }
  render() {
    return (
      <a href="#" className="header__basket">
        <img src="./shopping-cart.png" alt="" />
      </a>
    );
  }
}

Basket.propTypes = {
  countBuyProducts: PropTypes.number,
};
Basket.defaultProps = {
  countBuyProducts: 0,
};

export default Basket;

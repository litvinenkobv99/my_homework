import { Component } from "react";
import "./Favorites.scss";
import PropTypes from "prop-types";

class Favorites extends Component {
  constructor() {
    super();

    this.state = {
      favoritesProducts: [],
      onClick: false,
    };
  }
  componentDidMount = () => {
    if (localStorage.getItem("arrfavoritesProducts")) {
      const arr = JSON.parse(localStorage.getItem("arrfavoritesProducts"));
      arr !== null &&
        arr.forEach((element) => {
          if (JSON.stringify(element) == JSON.stringify(this.props.product)) {
            this.setState({
              onClick: !this.state.onClick,
            });
          }
        });
    }
  };
  render() {
    return (
      <a
        onClick={() => {
          this.props.handleClick(this.props.product);
          this.setState({
            onClick: !this.state.onClick,
          });
        }}
        className="header__favorites"
      >
        {this.state.onClick ? (
          <img src="./favorite-check.png" />
        ) : (
          <img src="./favorite.png" alt="" />
        )}
      </a>
    );
  }
}

Favorites.propTypes = {
  handleClick: PropTypes.func,
  product: PropTypes.object,
};

export default Favorites;

import { Component } from "react";
import Basket from "../Basket/Basket";
import Favorites from "../Favorites/Favorites";
import "./Header.scss";
import PropTypes from "prop-types";

class Header extends Component {
  constructor() {
    super();
  }

  render() {
    return (
      <header className="header">
        <div className="header__logo">
          <img src="./1.svg" alt="" />
        </div>
        <div>
          <Basket countBuyProducts={this.props.countBuyProducts} />
          {localStorage.getItem("productsToBuy") &&
            JSON.parse(localStorage.getItem("productsToBuy")).length}
          <Favorites />
          {localStorage.getItem("arrfavoritesProducts") &&
            JSON.parse(localStorage.getItem("arrfavoritesProducts")).length}
        </div>
      </header>
    );
  }
}

Header.propTypes = {
  countBuyProducts: PropTypes.number,
};
Header.defaultProps = {
  countBuyProducts: 0,
};

export default Header;

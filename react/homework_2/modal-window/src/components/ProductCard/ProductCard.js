import { Component } from "react";
import Button from "../Button/Button";
import "./ProductCard.scss";
import Favorites from "../Favorites/Favorites";
import PropTypes from "prop-types";

class ProductCard extends Component {
  constructor() {
    super();
    this.state = {
      onFavorite: false,
    };
  }

  render() {
    const { name, price, url, vendorCode, color, handleClick } =
      this.props.product;

    return (
      <li className="products__product-item">
        <img className="products__product-item-img" src={url}></img>
        <div className="products__product-item-name"> {name}</div>
        <div className="products__product-item-price"> {price}₴</div>
        <div className="products__product-item-color">Колір: {color}</div>
        <div>
          <Favorites
            product={this.props.product}
            handleClick={this.props.handleClick}
          />
          <Button
            text="Add to Card"
            style={{ backgroundColor: "purple" }}
            openModal={this.props.openModal}
            dataModalId={0}
            product={this.props.product}
          />
        </div>
      </li>
    );
  }
}

ProductCard.propTypes = {
  countFavoritesProducts: PropTypes.number,
  favoritesProducts: PropTypes.array,
  handleClick: PropTypes.func,
  openModal: PropTypes.func,
  product: PropTypes.object,
};

ProductCard.defaultProps = {
  countFavoritesProducts: 0,
  product: [],
};

export default ProductCard;

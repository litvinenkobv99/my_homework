import { Component } from "react";
import ProductCard from "../ProductCard/ProductCard";
import "./ProductList.scss";
import PropTypes from "prop-types";

let arrfavoritesProducts;

class ProductList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      products: [],
      favoritesProducts: [],
      countFavoritesProducts: 0,
    };
  }
  componentDidMount() {
    fetch("/arrProducts.JSON")
      .then((response) => response.json())
      .then((result) => {
        this.setState({
          products: result,
        });
      });
    if (localStorage.getItem("arrfavoritesProducts")) {
      this.setState({
        favoritesProducts: JSON.parse(
          localStorage.getItem("arrfavoritesProducts")
        ),
      });
    } else {
      this.setState({
        favoritesProducts: [
          localStorage.setItem("arrfavoritesProducts", JSON.stringify([])),
        ],
      });
    }
  }
  handleClick = (product) => {
    arrfavoritesProducts = this.state.favoritesProducts;
    console.log(arrfavoritesProducts.length);
    if (!arrfavoritesProducts.includes(product)) {
      arrfavoritesProducts.push(product);
      this.setState({
        favoritesProducts: arrfavoritesProducts,
        countFavoritesProducts: this.state.countFavoritesProducts + 1,
      });
      localStorage.arrfavoritesProducts = JSON.stringify(arrfavoritesProducts);
    } else {
      arrfavoritesProducts.forEach((elem, index) => {
        if (JSON.stringify(elem) == JSON.stringify(product)) {
          arrfavoritesProducts.splice(index, 1);
        }
      });
      this.setState({
        favoritesProducts: arrfavoritesProducts,
        countFavoritesProducts: this.state.countFavoritesProducts + 1,
      });
      localStorage.arrfavoritesProducts = JSON.stringify(arrfavoritesProducts);
    }
  };
  render() {
    return (
      <ul
        className="products"
        style={{ maxWidth: "1200px", margin: "0 auto " }}
      >
        {this.state.products.map((product) => {
          return (
            <ProductCard
              key={product.vendorCode}
              product={product}
              openModal={this.props.openModal}
              handleClick={this.handleClick}
              favoritesProducts={this.state.favoritesProducts}
              countFavoritesProducts={this.state.countFavoritesProducts}
            />
          );
        })}
      </ul>
    );
  }
}

ProductList.propTypes = {
  favoritesProducts: PropTypes.array,
  openModal: PropTypes.func,
};

ProductList.defaultProps = {
  favoritesProducts: [],
};

export default ProductList;

import React from "react";
import ReactDOM from "react-dom/client";
import App from "./components/App/App";

const root = ReactDOM.createRoot(document.getElementById("root"));
// console.log(arrProducts);
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

// fetch("/arrProducts.JSON")
//   .then((response) => response.json())
//   .then((result) => console.log(result));

import React, { useState, useEffect } from "react";
import Modal from "../Modal/Modal";
import "../../nullstyles.css";
import arrModalWindows from "../../arrModalWindow.js";
import { Route, Routes } from "react-router-dom";
import BasketProducts from "../../pages/BasketProducts/BasketProducts";
import Home from "../../pages/Home/Home.js";
import FavoritesProducts from "../../pages/FavoritesProducts/FavoritesProducts.js";
import Header from "../Header/Header";

const App = () => {
  const [modal, setModal] = useState({
    isOpen: false,
    modalWindow: {},
    currentProduct: {},
  });
  const [arrModalWindow, setArrModalWindow] = useState(arrModalWindows);
  const [arrProducts, setArrProducts] = useState([]);
  const [arrFavoriteProducts, setArrFavoriteProducts] = useState([]);
  const [arrProductsToBuy, setArrProductsToBuy] = useState([]);

  useEffect(() => {
    localStorage.getItem("arrFavorite")
      ? setArrFavoriteProducts(getFavoritesFromLS())
      : localStorage.setItem("arrFavorite", JSON.stringify([]));

    localStorage.getItem("arrProductsToBuy")
      ? setArrProductsToBuy(getProductsToBuyFromLS())
      : localStorage.setItem("arrProductsToBuy", JSON.stringify([]));

    fetch("/arrProducts.JSON")
      .then((response) => response.json())
      .then((result) => {
        setArrProducts(() => {
          return result;
        });
      });
  }, []);

  const openModal = (id, product) => {
    setModal(() => {
      return {
        isOpen: true,
        modalWindow: arrModalWindow[id],
        currentProduct: product,
      };
    });
  };

  const closeModal = () => {
    setModal(() => {
      return {
        ...modal,
        isOpen: false,
      };
    });
  };

  const addProductsToBuy = () => {
    if (!arrProductsToBuy.includes(modal.currentProduct)) {
      setArrProductsToBuy(() => {
        localStorage.setItem(
          "arrProductsToBuy",
          JSON.stringify([...arrProductsToBuy, modal.currentProduct])
        );
        return [...arrProductsToBuy, modal.currentProduct];
      });
    } else {
      setArrProductsToBuy(() => {
        let arr = arrProductsToBuy.filter((product) => {
          if (product.length !== modal.currentProduct.length) {
            return product;
          }
        });
        localStorage.setItem("arrProductsToBuy", JSON.stringify(arr));
        return arr;
      });
    }
    closeModal();
  };

  const addProductsToFavorites = (productToFavorites) => {
    if (!arrFavoriteProducts.includes(productToFavorites)) {
      setArrFavoriteProducts(() => {
        localStorage.setItem(
          "arrFavorite",
          JSON.stringify([...arrFavoriteProducts, productToFavorites])
        );
        return [...arrFavoriteProducts, productToFavorites];
      });
    } else {
      setArrFavoriteProducts(() => {
        let arr = arrFavoriteProducts.filter((product) => {
          if (JSON.stringify(product) == JSON.stringify(productToFavorites)) {
            // долго думал на счет этого, не понял, почему не стоит так делать?
            return false;
          } else {
            return true;
          }
        });
        localStorage.setItem("arrFavorite", JSON.stringify(arr));
        return arr;
      });
    }
  };

  const getFavoritesFromLS = () => {
    try {
      const favorites = localStorage.getItem("arrFavorite");
      const parsed = JSON.parse(favorites);
      return parsed;
    } catch {
      return [];
    }
  };

  const getProductsToBuyFromLS = () => {
    try {
      const productsToBut = localStorage.getItem("arrProductsToBuy");
      const parsed = JSON.parse(productsToBut);
      return parsed;
    } catch {
      return [];
    }
  };

  const { id, title, text, closeButton, actions } = modal.modalWindow;

  return (
    <>
      <Header
        arrFavoriteProducts={arrFavoriteProducts}
        arrProductsToBuy={arrProductsToBuy}
      />
      <Routes>
        <Route
          path="/basket"
          element={
            <BasketProducts
              arrFavoriteProducts={arrFavoriteProducts}
              arrProductsToBuy={arrProductsToBuy}
              addProductsToFavorites={addProductsToFavorites}
              openModal={openModal}
            />
          }
        />
        <Route
          path="/favorites"
          element={
            <FavoritesProducts
              arrFavoriteProducts={arrFavoriteProducts}
              arrProductsToBuy={arrProductsToBuy}
              addProductsToFavorites={addProductsToFavorites}
              openModal={openModal}
            />
          }
        />
        <Route
          path="/"
          element={
            <Home
              arrFavoriteProducts={arrFavoriteProducts}
              arrProductsToBuy={arrProductsToBuy}
              arrProducts={arrProducts}
              openModal={openModal}
              addProductsToFavorites={addProductsToFavorites}
            />
          }
        />
      </Routes>
      {modal.isOpen && (
        <Modal
          id={id}
          title={title}
          text={text}
          closeButton={closeButton}
          actions={actions}
          closeModal={closeModal}
          addProductsToBuy={addProductsToBuy}
        />
      )}
    </>
  );
};

export default App;

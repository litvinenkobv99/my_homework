import { Component } from "react";
import "./Basket.scss";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

// class Basket extends Component {
//   constructor() {
//     super();
//   }
//   render() {
//     return (
//       <Link to="/basket" className="header__basket">
//         <img src="./img/shopping-cart.png" alt="" />
//         {this.props.arrProductsToBuy.length}
//       </Link>
//     );
//   }
// }

const Basket = (props) => {
  return (
    <Link to="/basket" className="header__basket">
      <img src="./img/shopping-cart.png" alt="" />
      {props.arrProductsToBuy.length}
    </Link>
  );
};

Basket.propTypes = {
  arrProductsToBuy: PropTypes.array,
};

export default Basket;

import React, { useState, useEffect } from "react";
import "./Favorites.scss";
import PropTypes from "prop-types";

const Favorites = (props) => {
  const [onClick, setOnClick] = useState(false);
  useEffect(() => {
    JSON.parse(localStorage.getItem("arrFavorite")).forEach((element) => {
      if (JSON.stringify(element) == JSON.stringify(props.productToFavorites)) {
        setOnClick(() => {
          return !onClick;
        });
      }
    });
  }, []);
  const { addProductsToFavorites, productToFavorites } = props;
  return (
    <a
      onClick={() => {
        addProductsToFavorites(productToFavorites);
        setOnClick(() => {
          return !onClick;
        });
      }}
      className="header__favorites"
    >
      {onClick ? (
        <img src="./img/favorite-check.png" />
      ) : (
        <img src="./img/favorite.png" alt="" />
      )}
    </a>
  );
};

Favorites.propTypes = {
  addProductsToFavorites: PropTypes.func,
  product: PropTypes.object,
  productToFavorites: PropTypes.object,
};

export default Favorites;

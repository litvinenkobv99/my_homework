import React, { useState } from "react";
import "./Modal.scss";
import PropTypes from "prop-types";

const Modal = (props) => {
  console.log(props);
  const { id, title, text, closeButton, closeModal, addProductsToBuy } = props;
  return (
    <div className="modal" onClick={() => props.closeModal()}>
      <div
        className="modal__content"
        onClick={(e) => e.stopPropagation()}
        key={id}
      >
        <div className="modal__title">{title}</div>
        {closeButton && (
          <button onClick={() => closeModal()} className="modal__close-button">
            +
          </button>
        )}
        <div className="modal__text">{text}</div>
        <div className="modal__button-confirm">
          <>
            {id == 0 && (
              <button
                onClick={() => addProductsToBuy()}
                className="modal__button-ok"
                value={true}
              >
                Так
              </button>
            )}
            {id == 1 && (
              <button
                onClick={() => addProductsToBuy()}
                className="modal__button-ok"
                value={true}
              >
                Так
              </button>
            )}

            <button
              onClick={() => closeModal()}
              className="modal__button-cancel"
              value={false}
            >
              Ні
            </button>
          </>
        </div>
      </div>
    </div>
  );
};

Modal.propTypes = {
  addProductsToBuy: PropTypes.func,
  closeButton: PropTypes.bool,
  closeModal: PropTypes.func,
  id: PropTypes.number,
  text: PropTypes.string,
  title: PropTypes.string,
};
Modal.defaultProps = {
  title: "Додати товар до кошику?",
};

export default Modal;

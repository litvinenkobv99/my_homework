import ProductCard from "../../components/ProductCard/ProductCard.js";
import "./BasketProducts.scss";
import { useSelector } from "react-redux";

const BasketProducts = (props) => {
  const arrProductsToBuy = useSelector((state) => state.productsToBuy);
  return (
    <>
      <ul
        className="products"
        style={{ maxWidth: "1200px", margin: "0 auto " }}
      >
        {arrProductsToBuy.map((product) => {
          return (
            <ProductCard
              key={product.vendorCode}
              product={product}
              dataModalId={1}
              deleteCard={true}
              openModal={props.openModal}
              disabledBtn={true}
            />
          );
        })}
      </ul>
    </>
  );
};

export default BasketProducts;

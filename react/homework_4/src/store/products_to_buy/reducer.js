import { ADD_PRODUCT_TO_BUY, REMOVE_PRODUCT_TO_BUY } from "./type";

export const productsToBuyReducer = (state = [], action) => {
  switch (action.type) {
    case ADD_PRODUCT_TO_BUY:
      return [...action.payload];
    case REMOVE_PRODUCT_TO_BUY:
      return [...action.payload];
    default:
      return state;
  }
};

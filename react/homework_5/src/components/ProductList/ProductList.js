import "./ProductList.scss";
import ProductCard from "../ProductCard/ProductCard";
import PropTypes from "prop-types";
import { useSelector } from "react-redux";

const ProductList = (props) => {
  const arrProducts = useSelector((state) => state.products.products);
  const { openModal } = props;

  return (
    <ul className="products" style={{ maxWidth: "1200px", margin: "0 auto " }}>
      {arrProducts.map((product) => {
        return (
          <ProductCard
            key={product.vendorCode}
            product={product}
            openModal={openModal}
          />
        );
      })}
    </ul>
  );
};

ProductList.propTypes = {
  arrProducts: PropTypes.array,
  openModal: PropTypes.func,
};

export default ProductList;
